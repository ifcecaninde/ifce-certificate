# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ifce/certificate/version'

Gem::Specification.new do |spec|
  spec.name          = "ifce-certificate"
  spec.version       = IFCE::Certificate::VERSION
  spec.authors       = ["Carlos Alberto Castelo Elias Filho"]
  spec.email         = ["cacefmail@gmail.com"]
  spec.summary       = %q{Gerador de Certificados do IFCE - Campus Canindé}
  spec.description   = %q{Gerador de Certificados do IFCE - Campus Canindé}
  spec.homepage      = ""
  spec.license       = "IFCE"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "prawn", "~> 1.2.1"

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
end
