# Gerador de Certificados IFCE

Gem criada com o objetivo de facilitar a geração de certificados de participação de eventos e outros
fins, utilizando o padrão adotado pelo IFCE - Campus Canindé

## Instalação

Adicione esta linha no Gemfile de sua aplicação:

```ruby
gem 'ifce-certificate', :git => 'https://bitbucket.org/ifcecaninde/ifce-certificate.git'
```

E depois execute:

    $ bundle install

## Utilização

1. Importe a gem usando require (`require 'ifce/certificate'`)
2. Crie uma nova fábrica de certificados (`factory = IFCE::Certificate::Factory.new(texto: "Certificamos que fulano completou etc, etc, etc", date: "Canindé, 13 de Março de 2015", campus: "<i>Campus</i> Canindé")`)
3. Anexe as assinaturas (`factory.signatures.push({image: "assinaturas/modelo.png", text: ["Modelo de Assinatura", "IFCE - <i>Campus</i> Canindé"]})`)
4. Gere e salve o PDF (`factory.savePDF("exemplo.pdf")`)

O exemplo abaixo gera um Certificado de exemplo com duas assinaturas, e o salva com o nome de "exemplo.pdf"

```ruby
require 'ifce/certificate'

#configura o texto do certificado (note que podemos usar algumas tags HTML de formatação)
texto = "Certificamos que <b>Fulano de Tal</b> participou da " +
          "palestra do <b>Sistema Gerador de Certificados</b>," +
          " realizado em Canindé-CE, no dia 16 de Setembro de 2014."

#configura o local e data
data = "Canindé, 17 de Setembro de 2014."

#cria um novo certificado com os dados estabelecidos anteriormente
certificado = IFCE::Certificate::Factory.new(text: texto, date: data)

#gera uma assinatura do Diretor Geral
vidal = {image: "assinaturas/vidal.png", text: ["Diretor Geral", "IFCE - <i>Campus</i> Canindé"]}

#gera outra assinatura
teste = {image: "assinaturas/modelo.png", text: ["Modelo de Assinatura", "IFCE - <i>Campus</i> Canindé"]}

#acrescenta as duas assinaturas ao certificado
certificado.options[:signatures].push vidal
certificado.options[:signatures].push teste

#gera o arquivo .pdf
certificado.savePDF("exemplo.pdf")
```

## Mais Opções

Na criação da fábrica, você poderá passar uma Hash contendo todas as opções que queira inicializar ex:

`factory = IFCE::Certificate::Factory.new(campus: "<i>Campus</i> Canindé")`

mas se for necessário, poderá modificar essas opções diretamente pela propriedade "options" ex:

`factory.options[:campus] = "<i>Campus</i> Canindé"`

Veja abaixo a lista das opções possíveis

| opção      | explicação                            | valor padrão            |
|------------|---------------------------------------|-------------------------|
| title      | Título do documento                   | "CERTIFICADO"           |
| title_size | Tamanho da Fonte do Título            | 32                      |
| campus     | Campus que está gerando o certificado | "<i>Campus</i> Canindé" |
| text       | Texto do Certificado                  | ""                      |
| text_size  | Tamanho da fonte do texto             | 18                      |
| date       | Local e data de assinatura            | ""                      |
| signatures | Array contendo as assinaturas         |                         |

## Assinaturas

Cada assinatura é um Hash contendo duas chaves: "image" e "text".

"image" - o caminho para o arquivo de imagem da assinatura digital

"text" - o texto que acompanha a assinatura em forma de String ou Array, cada item do Array corresponde a uma linha de texto

Para melhores resultados, usar o modelo do Photoshop ([https://bitbucket.org/ifcecaninde/ifce-certificate/downloads/modelo.psd](https://bitbucket.org/ifcecaninde/ifce-certificate/downloads/modelo.psd)) para alinhar corretamente as assinaturas no template padrão

##Formatação HTML

Qualquer string usada na criação do certificado pode usar tags HTML para formatar o texto, como `<i></i>` para escrever em itálico