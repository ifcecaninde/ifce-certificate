require 'spec_helper'
module IFCE::Certificate
  describe Factory do
    context 'creation' do
      it 'objeto criado sem nenhuma opção definida, retorna opções padrão' do
        factory = Factory.new()
        expect(factory.options).to match(Factory::DEFAULT_OPTIONS)
      end
      it 'objeto criado com opção definida, retorna opções padrão + aquela opção' do
        r = Factory::DEFAULT_OPTIONS.merge(campus: "Reitoria")
        factory = Factory.new(campus: "Reitoria")
        expect(factory.options).to match(r)
      end
    end
    context 'building PDF' do
      after(:each) do
        if File.exist?("test/test_file.pdf")
          FileUtils.rm("test/test_file.pdf")
        end
      end
      it 'save PDF' do
        factory = Factory.new(
          campus: "<i>campus</i> Canindé",
          text: "Certificamos que <b>CARLOS ALBERTO CASTELO ELIAS FILHO</b> participou da " +
          "criação do <b>Script de Construção de Certificados</b>, " +
          "cujo primeiro teste foi realizado em Canindé-CE, no dia 16 de Setembro de 2014.",
          date: "Canindé, 13 de março de 2015",
          signatures: [
              {
                text: ['Carlos Alberto Castelo Elias Filho', 'Coordenador de Tecnologia da Informação', '<i>Campus</i> Canindé']
              }
            ]
          )
        file = "test/test_file.pdf"
        factory.savePDF(file)
        expect(File.exist?(file)).to be_truthy
      end
    end
  end
end
