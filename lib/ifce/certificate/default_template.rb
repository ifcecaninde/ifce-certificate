require "prawn"
class IFCE::Certificate::DefaultTemplate < IFCE::Certificate::Template
  def buildPDF(data)
    Prawn::Document.new(:page_size   => "A4", :page_layout => :landscape) do
      #primeira borda (verde escuro)
      #fill_color "92D050"
      #35972F
      fill_color "35972F"
      fill   { rectangle [0, bounds.top], bounds.right, bounds.top }
      #segunda borda (verde claro)
      #8fd88a
      #fill_color "00B050"
      fill_color "8FD88A"
      x_margin = 25
      fill   { rounded_rectangle [x_margin, bounds.top - x_margin], bounds.right - (x_margin * 2), bounds.top - (x_margin * 2), 20 }
      #parte interna
      bounding_box([x_margin * 2, bounds.top - x_margin * 2], :width => bounds.right - (x_margin * 4), :height => bounds.top - (x_margin * 4)) do
        #quadro branco central
        fill_color "FFFFFF"
        fill   { rectangle [0, bounds.top], bounds.right, bounds.top }
        #Imagem de fundo
        tcursor = cursor
        image File.join(File.dirname(File.expand_path(__FILE__)), "images/logo.png"), :position => :center, :vposition => :center, :fit => [200, 200]
        move_cursor_to tcursor
        #cabeçalho
        image File.join(File.dirname(File.expand_path(__FILE__)), "images/brasao.png"), :position => :center, :fit => [50, 50],:vposition => 5
        stroke_color "FF0000"
        stroke_bounds
        fill_color "000000"
        move_down 10
        font "Helvetica"
        font_size 11
        text "República Federativa do Brasil", :align => :center
        text "Ministério da Educação", :align => :center
        text "Secretaria de Educação Profissional e Tecnológica", :align => :center
        text "Instituto Federal  de Educação, Ciência e Tecnologia do Ceará", :align => :center
        text data[:campus], :align => :center, :inline_format => true
        font "Times-Roman"
        move_down 10
        font_size data[:title_size]
        text data[:title], :align => :center, style: :bold

        bounding_box([x_margin, cursor], :width => bounds.right - x_margin * 2) do
          font data[:text_font]
          font_size data[:text_size]
          text "#{Prawn::Text::NBSP * 10}" + "#{data[:text]}", :align => :justify, :inline_format => true
          move_down 10
          text "#{data[:date]}", :align => :right
        end

        def buildSignature(signature)
          #TODO: Limpar e documentar o código
          image = signature[:image]
          texto = signature[:text]
          texto = texto.join("\n") if texto.kind_of?(Array)
          arquivo = image
          unless image.nil? or File.exists?(image)
            if File.exists?(File.join(File.dirname(File.expand_path(__FILE__)), image))
              arquivo = File.join(File.dirname(File.expand_path(__FILE__)), image)
            else
              arquivo = nil
            end
          end
          unless arquivo.nil?
            tcursor = cursor
            image arquivo, :position => :center, :vposition => :top, :fit => [bounds.width, 200]
            move_cursor_to tcursor
          end
          stroke_color "000000"
          fill_color "000000"
          stroke do
            move_to bounds.left, bounds.top - 37
            line_to bounds.right, bounds.top - 37
          end
          move_down 40
          font "Helvetica"
          font_size 11
          #textos.each do |texto|
          text texto, :align => :center, :inline_format => true
          #end
        end
        unless data[:signatures].nil?
          bounding_box([x_margin, 125 - x_margin], :width => bounds.right - x_margin * 2, :height => 125 - x_margin) do
            define_grid(:columns => 3, :rows => 1, :gutter => 10)
            if data[:signatures].length == 1
              #imprime uma única assinatura no centro
              grid([0, 1], [0,1]).bounding_box do
                buildSignature(data[:signatures][0])
              end
            elsif data[:signatures].length == 2
              #imprime uma assinatura no lado esquerdo, depois no direito
              grid([0, 0], [0,0]).bounding_box do
                buildSignature(data[:signatures][0])
              end
              grid([0, 2], [0,2]).bounding_box do
                buildSignature(data[:signatures][1])
              end
            elsif data[:signatures].length == 3
              #imprime três assinaturas
              grid([0, 0], [0,0]).bounding_box do
                buildSignature(data[:signatures][0])
              end
              grid([0, 1], [0,1]).bounding_box do
                buildSignature(data[:signatures][1])
              end
              grid([0, 2], [0,2]).bounding_box do
                buildSignature(data[:signatures][2])
              end
            end
          end
        end
      end
    end
  end
end
