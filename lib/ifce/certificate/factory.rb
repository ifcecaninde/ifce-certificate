class IFCE::Certificate::Factory
  DEFAULT_OPTIONS = {
    title: "CERTIFICADO",
    campus: "<i>Campus</i> Canindé",
    title_size: 32,
    text_font: "Helvetica",
    text: "",
    text_size: 18,
    date: "",
    signatures: nil,
    template: nil,
  }
  attr_accessor :options
  #Inicializa a Factory com as opções estabelecidas
  def initialize(options={})
    #mescla as opções com as padrão
    @options = DEFAULT_OPTIONS
    @options[:signatures] = []
    @options.merge!(options)
  end

  #Gera um PDF com esses dados e o template indicado
  #Se não for indicado um template agora, gera utilizando o template definido nas opções
  def buildPDF(template=nil)
    #caso o template seja nil, usa o template das opções
    template = @options[:template] if template.nil?
    #caso o template nas opções também seja nil, usa o template padrão
    template = IFCE::Certificate::DefaultTemplate.new() if template.nil?

    template.buildPDF(@options)
  end

  #Gera e salva um PDF usando o caminho e nome do arquivo indicado, também usa o template, conforme buildPDF
  def savePDF(file, template=nil)
    pdf = buildPDF(template)
    #verifica se a pasta de destino existe, caso não exista, cria
    FileUtils.mkdir_p File.dirname(file)
    #finalmente salva o arquivo para a pasta
    pdf.render_file file
  end
end
